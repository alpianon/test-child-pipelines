# test-child-pipelines

testing child pipelines with resource_group in order to prevent some jobs to be executed in multiple concurrent pipelines, like explained here:

https://docs.gitlab.com/ee/ci/resource_groups/index.html#pipeline-level-concurrency-control-with-cross-projectparent-child-pipelines

(since it is the only available workaround to that purpose as of now, see [here](https://gitlab.com/gitlab-org/gitlab/-/issues/215034#note_623565474))

To pass dotenv artifact to child pipelines we set a PARENT_PIPELINE_ID variable in the parent pipeline, like suggested here:

https://docs.gitlab.com/ee/ci/yaml/#needspipelinejob

